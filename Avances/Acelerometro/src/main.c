#include <i2c.h>
#include "chip.h"

// Dirección del acelerometro
#define SLAVE_ADDRESS	0x68


//Registros
#define	PWR_MGMT_1 		0x6B
#define WHOAMI 			0x75
#define	ACCEL_XOUT_H	0x3B
#define ACCEL_XOUT_L	0x3C
#define	ACCEL_YOUT_H	0x3D
#define	ACCEL_YOUT_L 	0x3E
#define	ACCEL_ZOUT_H	0x3F
#define	ACCEL_ZOUT_L	0x40


/* Buffer de escritura y escritura del i2c*/
unsigned char I2C_Buffer_Tx[] = {0,0};
unsigned char I2C_Buffer_Rx[] = {0,0,0,0,0,0};

//Variables de aceleracion 8 bits con signo
int8_t aceleracionX = 0;
int8_t aceleracionY = 0;
int8_t aceleracionZ = 0;


int main(void) {

    SystemCoreClockUpdate();

    init_i2c();

    //Saco al acelerometro del modo sleep
    I2C_Buffer_Tx[0] = PWR_MGMT_1;
    I2C_Buffer_Tx[1] = 0x00;
    Chip_I2C_MasterSend (I2C1, SLAVE_ADDRESS,I2C_Buffer_Tx,2);

    /*Lectura del id para chequear correcto funcionamiento
	I2C_Buffer_Tx[0] = WHOAMI;
	Chip_I2C_MasterSend(I2C1, SLAVE_ADDRESS,I2C_Buffer_Tx,1);
	Chip_I2C_MasterRead(I2C1, SLAVE_ADDRESS,I2C_Buffer_Rx, 1);
	*/

    while(1){
    lecturaacelerometro();
    }

    return 0 ;
}


void lecturaacelerometro(void){

	I2C_Buffer_Tx[0] = ACCEL_XOUT_H;
	Chip_I2C_MasterSend(I2C1, SLAVE_ADDRESS,I2C_Buffer_Tx,1);
	Chip_I2C_MasterRead(I2C1, SLAVE_ADDRESS,I2C_Buffer_Rx, 6);
	//aceleracionX = (I2C_Buffer_Rx[0] << 8) + I2C_Buffer_Rx[1];
	aceleracionX = I2C_Buffer_Rx[0];
	aceleracionY = I2C_Buffer_Rx[2];
	aceleracionZ = I2C_Buffer_Rx[4];

	if(aceleracionX < 0)
		aceleracionX = (-1)*aceleracionX;
	if(aceleracionY < 0)
		aceleracionY = (-1)*aceleracionY;
	if(aceleracionZ < 0)
		aceleracionZ = (-1)*aceleracionZ;
}
