#include "chip.h"
#include "string.h"

#define UART_SELECTION 	LPC_UART3
#define IRQ_SELECTION 	UART3_IRQn
#define HANDLER_NAME 	UART3_IRQHandler

STATIC RINGBUFF_T txring, rxring;

#define UART_SRB_SIZE 256	/* Send */
#define UART_RRB_SIZE 256	/* Receive */

static uint8_t rxbuff[UART_RRB_SIZE], txbuff[UART_SRB_SIZE];

void init_uart(void);
int SerialDisponible(void);

//Variables que envio
const char inst1[] = "Las coordenadas de gps son: \r\n";
uint8_t lagra[5],lamin[10],lacoor,longra[5],lonmin[10],loncoor;
uint8_t data[80];
uint8_t x[400];
uint8_t xy[40];
uint8_t salgo1,a=0,i=0,y=0,z=0,datox=0,listo=0;
const char espacio[] = " \r";
const char fin[] = "\r\n";


uint8_t key;
int bytes;
char key2;

void HANDLER_NAME(void){
	Chip_UART_IRQRBHandler(UART_SELECTION, &rxring, &txring);
}

int main(void){


 	SystemCoreClockUpdate();

	init_uart();

	while(1){

	while (SerialDisponible()){
		if(i>400)
		i=0;
		//Chip_UART_ReadRB(UART_SELECTION, &rxring, &key, 1);
		x[i] = key;
	    if(x[i-5]==71 && x[i-4]==80 && x[i-3]==71 && x[i-2]==71 && x[i-1]==65){
	    	salgo1 = 1;
	    	i=0;
	    	listo = 1;
	    	while(salgo1){
	    		while (SerialDisponible()){
	    			//Chip_UART_ReadRB(UART_SELECTION, &rxring, &key, 1);
	    			xy[i] = key;
	    			if(xy[i]==44){
	    				salgo1 = 0;
	    				break;
	    			}
	    			i++;
	    		}
	    	}

	    	i=0;
	    	datox=0;
	    	while(datox < 4){
	    		while (SerialDisponible()){
	    			//Chip_UART_ReadRB(UART_SELECTION, &rxring, &key, 1);
	    			data[i] = key;
	    			if(data[i]==44){
	    				datox++;
	    				i++;
	    				break;
	    			}
	    			i++;
	    		}
	    	}
	    	}
	    	i++;
	}

	if(listo == 1){
		a = 0;
		i = 0;
		while(a < 2){
			if(data[i]==78 || data[i]==83 || data[i]==87 || data[i]==69){
				a++;
				if(a==1){
					lacoor = data[i];
					y = i-1;
				}
				else{
					z = i-1;
					loncoor = data[i];
				}
			}
		i++;
		}
	for(i=0;i<2;i++){
		lagra[i] = data[i];
	}

	for(i=2;i<y;i++){
		lamin[i-2] = data[i];
	}


	for(i=y+3;i<y+6;i++){
		longra[i-13]= data[i];
	}


	for(i=y+6;i<z;i++){
		lonmin[i-16] = data[i];
	}

	listo = 0;

	Chip_UART_SendRB(UART_SELECTION, &txring, inst1, sizeof(inst1) - 1);
	Chip_UART_SendRB(UART_SELECTION, &txring, lagra, sizeof(lagra));
	Chip_UART_SendRB(UART_SELECTION, &txring, espacio, sizeof(espacio) - 1);
	Chip_UART_SendRB(UART_SELECTION, &txring, lamin, sizeof(lamin));
	Chip_UART_SendRB(UART_SELECTION, &txring, espacio, sizeof(espacio) - 1);
	//Chip_UART_SendByte(UART_SELECTION,212);
	Chip_UART_SendRB(UART_SELECTION, &txring, &lacoor, sizeof(lacoor));
	Chip_UART_SendRB(UART_SELECTION, &txring, espacio, sizeof(espacio) - 1);
	Chip_UART_SendRB(UART_SELECTION, &txring, longra, sizeof(longra));
	Chip_UART_SendRB(UART_SELECTION, &txring, espacio, sizeof(espacio) - 1);
	Chip_UART_SendRB(UART_SELECTION, &txring, lonmin, sizeof(lonmin));
	Chip_UART_SendRB(UART_SELECTION, &txring, espacio, sizeof(espacio) - 1);
	Chip_UART_SendRB(UART_SELECTION, &txring, &loncoor, sizeof(loncoor));
	Chip_UART_SendRB(UART_SELECTION, &txring, fin, sizeof(fin) - 1);
	while(1);
	}
	}
	return 1;
}

void init_uart(void){

		Chip_IOCON_PinMuxSet(LPC_IOCON,0,0,FUNC2);   //Asigno el pin P0.0 como TXD3
		Chip_IOCON_PinMuxSet(LPC_IOCON,0,1,FUNC2);   //Asigno el pin P0.1 como RXD3

		//configuracion del uart
		Chip_UART_Init(UART_SELECTION);
		Chip_UART_SetBaud(UART_SELECTION, 9600);
		Chip_UART_ConfigData(UART_SELECTION, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
		Chip_UART_SetupFIFOS(UART_SELECTION, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV2));
		Chip_UART_TXEnable(UART_SELECTION);

		//inicializa el buffer
		RingBuffer_Init(&rxring, rxbuff, 1, UART_RRB_SIZE);
		RingBuffer_Init(&txring, txbuff, 1, UART_SRB_SIZE);

		Chip_UART_SetupFIFOS(UART_SELECTION, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
								UART_FCR_TX_RS | UART_FCR_TRG_LEV3));
		Chip_UART_IntEnable(UART_SELECTION, (UART_IER_RBRINT | UART_IER_RLSINT));


		NVIC_SetPriority(IRQ_SELECTION, 1);
		NVIC_EnableIRQ(IRQ_SELECTION);
}

int SerialDisponible(void){
	bytes = Chip_UART_ReadRB(UART_SELECTION, &rxring, &key, 1); //Guardo el dato en key
	if (bytes > 0)
	return 1;
	else
	return 0;
}
