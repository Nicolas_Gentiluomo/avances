#include "chip.h"
#include "string.h"
#include <timer.h>


#define UART_SELECTION 	LPC_UART3
#define IRQ_SELECTION 	UART3_IRQn
#define HANDLER_NAME 	UART3_IRQHandler

#define MAX_VALUE 1000 //Valor máximo de la rampa (<1024)
#define TIMER_TICK_RATE_HZ 1000 //Tick para actualizar el valor de la rampa
#define STEP 1
#define THRESHOLD 500

STATIC RINGBUFF_T txring, rxring;

#define UART_SRB_SIZE 256	/* Send */
#define UART_RRB_SIZE 256	/* Receive */

static uint8_t rxbuff[UART_RRB_SIZE], txbuff[UART_SRB_SIZE];

void init_uart(void);
void init_timer(void);
int SerialDisponible(void);
void lecturagsm(void);
void enviocoordenadas(void);
void init_gsm(void);


//Variables que envio
const char inst1[] = "AT\r\n";
const char inst2[] = "AT+IPR=9600\r\n";
const char inst3[] = "AT+CMGF=1\r\n";
const char inst4[] = "AT+CNMI=2,2,0,0,0\r\n";
char inst5[] = "AT+CMGS=\"+5491154884139\"\r\n";
const char inst6[] = "Aca van las coordenadas\r";
const char inst7[] = "AT+CMGS=";
const char inst8[] = "\n";
char numero[17];
char mensaje[27];


uint8_t sms[150];
uint8_t lectura[150];
uint8_t check[10];
uint8_t key;
int bytes;
int i = 0;
int a = 0;
int c = 0;
int z;
int error = 0;

void HANDLER_NAME(void){
	Chip_UART_IRQRBHandler(UART_SELECTION, &rxring, &txring);
}

void HANDLER_TIMER_NAME(void){

	static uint32_t value=0;

	if (Chip_TIMER_MatchPending(my_TIMER, MATCH0))
	{
		Chip_TIMER_ClearMatch(my_TIMER, MATCH0);

		if  (value < 500)
			value++;
		else{
			value=0;
			a=1;

		}
	}
}

int main(void){


 	SystemCoreClockUpdate();

	init_uart();
	init_timer();
	init_gsm();


	while(1){
		a = 0;
		while(a==0);
		lecturagsm();

	}
	return 1;
}

void init_uart(void){

		Chip_IOCON_PinMuxSet(LPC_IOCON,0,0,FUNC2);   //Asigno el pin P0.0 como TXD3
		Chip_IOCON_PinMuxSet(LPC_IOCON,0,1,FUNC2);   //Asigno el pin P0.1 como RXD3

		//configuracion del uart
		Chip_UART_Init(UART_SELECTION);
		Chip_UART_SetBaud(UART_SELECTION, 9600);
		Chip_UART_ConfigData(UART_SELECTION, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
		Chip_UART_SetupFIFOS(UART_SELECTION, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV2));
		Chip_UART_TXEnable(UART_SELECTION);

		//inicializa el buffer
		RingBuffer_Init(&rxring, rxbuff, 1, UART_RRB_SIZE);
		RingBuffer_Init(&txring, txbuff, 1, UART_SRB_SIZE);

		Chip_UART_SetupFIFOS(UART_SELECTION, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
								UART_FCR_TX_RS | UART_FCR_TRG_LEV3));
		Chip_UART_IntEnable(UART_SELECTION, (UART_IER_RBRINT | UART_IER_RLSINT));


		NVIC_SetPriority(IRQ_SELECTION, 1);
		NVIC_EnableIRQ(IRQ_SELECTION);
}

void init_timer(void){
	uint32_t TimerFreq;

	/* Habilita el timer elegido */
	Chip_TIMER_Init(my_TIMER);

	/* Levanta la frecuencia a la que corre el clock interno del timer */
    TimerFreq = Chip_Clock_GetPeripheralClockRate(my_TIMER_SPEED);

    /* Setea el timer en modo Match e Interrupción */
	Chip_TIMER_Reset(my_TIMER);
	Chip_TIMER_MatchEnableInt(my_TIMER, MATCH0);
	Chip_TIMER_SetMatch(my_TIMER, MATCH0, (TimerFreq / TIMER_TICK_RATE_HZ));

	/* Resetea en el match */
	Chip_TIMER_ResetOnMatchEnable(my_TIMER, MATCH0);

	/* Habilita la interrupción del timer */
	NVIC_EnableIRQ(my_TIMER_IRQ);
	NVIC_ClearPendingIRQ(my_TIMER_IRQ);

	/* Arranca el timer */
	Chip_TIMER_Enable(my_TIMER);
}

int SerialDisponible(void){
	bytes = Chip_UART_ReadRB(UART_SELECTION, &rxring, &key, 1); //Guardo el dato en key
	if (bytes > 0)
	return 1;
	else
	return 0;
}

void init_gsm(void){

	Chip_UART_SendRB(UART_SELECTION, &txring, inst1, sizeof(inst1) - 1);

	//Demora de 4 segundos. Sino puedo esperar hasta recibir el OK
	for(i = 0;i<9;i++){
		a = 0;
		while(a==0);
	}

	i = 0;
	while (SerialDisponible()){
		check[i] = key;
		i++;
	}

	/*
	if(check[0] == 79 && check[1] == 75){  //Chequeo si llego OK
		error = 0;
	}
	else{
		error = 1;
	}
	*/

	Chip_UART_SendRB(UART_SELECTION, &txring, inst2, sizeof(inst2) - 1);
	a = 0;
	while(a==0);
	Chip_UART_SendRB(UART_SELECTION, &txring, inst3, sizeof(inst3) - 1);
	a = 0;
	while(a==0);
	Chip_UART_SendRB(UART_SELECTION, &txring, inst4, sizeof(inst4) - 1);
	a = 0;
	while(a==0);
}

void lecturagsm(void){

	//Guardo el mensaje en sms
	c = 0;
	i = 0;
	while (SerialDisponible()){
		sms[i] = key;
		i++;
		c = 1;
		if(i>150)
			i = 150;
	}

	if(c == 1){
		//Leo el sms y me fijo si recibi "GPS"
		for(i=0;i<150;i++){
			if(sms[i] == 71 && sms[i+1] == 80 && sms[i+2] == 83){  //Chequeo si el mensaje contiene GPS
				obtengonumero();
				enviocoordenadas();
				break;
			}
		}
		for(i=0;i<150;i++){
			lectura[i] = sms[i];
		}

		//Borro sms
		for(i=0;i<150;i++){
			sms[i] = 0;
		}

	}
}

void enviocoordenadas(void){

	Chip_UART_SendRB(UART_SELECTION, &txring, inst5, sizeof(inst5) - 1);
	a = 0;
	while(a==0);
	Chip_UART_SendRB(UART_SELECTION, &txring, inst6, sizeof(inst6) - 1);
	a = 0;
	while(a==0);
	Chip_UART_SendByte(UART_SELECTION,26);


}

void obtengonumero(void){

	i = 0;
	while(sms[i] != 34){
		i++;
	}
	i++;
	i++;
	z = 10;
	inst5[z] = sms[i];
	i++;
	z++;
	while(sms[i] != 34){
		inst5[z] = sms[i];
		i++;
		z++;
	}

}
